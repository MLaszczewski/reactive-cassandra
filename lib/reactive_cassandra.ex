defmodule RactiveCassandra do
  use Application
  use Confex
  require Logger

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Define workers and child supervisors to be supervised
      # worker(RactiveCassandra.Worker, [arg1, arg2, arg3])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options

    :ok = :erlcass.set_cluster_options(Confex.get_map(:reactive_cassandra, :cluster_options))
    :ok = :erlcass.create_session(keyspace: Confex.get(:reactive_cassandra, :keyspace))

    Reactive.CassandraEntityDao.prepare()
    Reactive.CassandraIndexDao.prepare()
    Reactive.CassandraLogDao.prepare()

    opts = [strategy: :one_for_one, name: RactiveCassandra.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
