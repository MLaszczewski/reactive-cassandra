defmodule Reactive.CassandraEntityDao do
  require Logger

  def prepare() do
    :erlcass.execute(
              "CREATE TABLE entities (
                 entity_id text,
                 state blob,
                 container blob,
                 PRIMARY KEY(entity_id)
               );")
    :ok = :erlcass.add_prepare_statement(:load_entity,
      "SELECT state, container FROM entities WHERE entity_id = ? LIMIT 1")
    :ok = :erlcass.add_prepare_statement(:save_entity,
      "INSERT INTO entities(entity_id,state,container) VALUES(?,?,?)")
  end

  def load(id) do
    db_id = Reactive.CassandraEntityId.entity_db_id(id)
    {:ok,r} = :erlcass.execute(:load_entity, [db_id])
    Logger.debug("CASS RES #{inspect id} => #{inspect r}")
    case r do
      [] ->
        :not_found
      [{state_bin,container_bin}] ->
        {:ok,%{
          state: :erlang.binary_to_term(state_bin),
          container: :erlang.binary_to_term(container_bin)
        }}
    end
  end

  def store(id,data) do
    db_id = Reactive.CassandraEntityId.entity_db_id(id)
    {:ok,r} = :erlcass.execute(:save_entity, [db_id,:erlang.term_to_binary(data.state),:erlang.term_to_binary(data.container)])
    Logger.debug("CASS SAVE RES #{inspect id} => #{inspect r}")
    :ok
  end
  
end