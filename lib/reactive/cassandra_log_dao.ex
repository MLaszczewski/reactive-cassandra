defmodule Reactive.CassandraLogDao do
  require Logger

  def prepare() do
    :erlcass.execute(
            "CREATE TABLE logs (
               log_id text,
               message_id text,
               content blob,
               PRIMARY KEY((log_id), message_id)
             ) WITH CLUSTERING ORDER BY (message_id DESC);")
    :ok = :erlcass.add_prepare_statement(:add_to_log,
      "INSERT INTO logs(log_id, message_id, content) VALUES(?,?,?)")
    :ok = :erlcass.add_prepare_statement(:remove_from_log,
      "DELETE FROM logs WHERE log_id = ? AND message_id = ?")
    :ok = :erlcass.add_prepare_statement(:update_in_log,
      "UPDATE logs SET content= ? WHERE log_id = ? AND message_id = ?")
    :ok = :erlcass.add_prepare_statement(:get_from_log,
      "SELECT message_id, content FROM logs WHERE log_id = ? AND message_id = ?")
    :ok = :erlcass.add_prepare_statement(:log_fetch_all_rev,
      "SELECT message_id, content FROM logs WHERE log_id = ? AND message_id <= ? LIMIT ?")
    :ok = :erlcass.add_prepare_statement(:log_fetch_all_all_rev,
      "SELECT message_id, content FROM logs WHERE log_id = ? LIMIT ?")
  end

  def init(id) do
    id
  end

  def open(id) do
    id
  end

  def push(log,key,data) do
    Logger.debug('ADD TO LOG #{inspect log} #{inspect key} #{inspect data}')
    {:ok,r} = :erlcass.execute(:add_to_log, [Reactive.CassandraEntityId.entity_db_id(log),key,:erlang.term_to_binary(data)])
    r
  end

  def remove(log,key) do
    {:ok,r} = :erlcass.execute(:remove_from_log, [Reactive.CassandraEntityId.entity_db_id(log),key])
    r
  end

  def overwrite(log,key,data) do
    {:ok,r} = :erlcass.execute(:update_in_log, [:erlang.term_to_binary(data),Reactive.CassandraEntityId.entity_db_id(log),key])
    r
  end

  def fetch(log,from,to,limit,reverse) do
    if(reverse) do
      if(to == :begin) do
        res=if(from == :end) do
          {:ok,r} = :erlcass.execute(:log_fetch_all_all_rev, [Reactive.CassandraEntityId.entity_db_id(log),limit])
          r
        else
          {:ok,r} = :erlcass.execute(:log_fetch_all_rev, [Reactive.CassandraEntityId.entity_db_id(log),from,limit])
          r
        end
        rr=Enum.map(res,fn({k,v}) -> {k,:erlang.binary_to_term(v)} end)
        Logger.debug("FETCH LOG #{inspect log} #{inspect from} => #{inspect rr}")
        rr
      else
        throw "not implemented"
      end
    else
      throw "not implemented"
    end
  end

end