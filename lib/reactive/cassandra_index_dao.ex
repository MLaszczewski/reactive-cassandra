defmodule Reactive.CassandraIndexDao do
  require Logger

  def prepare() do
    :ets.new(:auto_indexes, [:public, :named_table, :set, { :keypos, 1 }])
  end

  def get_or_create_index(index_name) do
    ret = :ets.lookup(:auto_indexes, index_name)
    case ret do
      [] ->
        :erlcass.execute(
          "CREATE TABLE IF NOT EXISTS auto_index_"<>index_name<>" (
            key text,
            entity_id blob,
            PRIMARY KEY((key), entity_id)
          );")
        add_atom = :erlang.binary_to_atom("add_to_index_"<>index_name,:utf8)
        remove_atom = :erlang.binary_to_atom("remove_from_index_"<>index_name,:utf8)
        find_atom = :erlang.binary_to_atom("find_in_index_"<>index_name,:utf8)
        :ok = :erlcass.add_prepare_statement(add_atom,
          "INSERT INTO auto_index_"<>index_name<>"(key, entity_id) VALUES(?,?)")
        :ok = :erlcass.add_prepare_statement(remove_atom,
          "DELETE FROM auto_index_"<>index_name<>" WHERE key = ? AND entity_id = ?")
        :ok = :erlcass.add_prepare_statement(find_atom,
          "SELECT entity_id FROM auto_index_"<>index_name<>" WHERE key = ?")
        index = %{
          add: add_atom,
          remove: remove_atom,
          find: find_atom
        }
        :ets.insert(:auto_indexes,{index_name, index})
        index
      [{^index_name, index}] ->
        index
    end
  end

  def add(index_name, keys, id) when is_list(keys) do
    index = get_or_create_index(index_name)
    db_id = :erlang.term_to_binary(id) #Reactive.CassandraEntityId.entity_db_id(id)
    for(key <- keys) do
      {:ok,r} = :erlcass.execute(index.add, [key,db_id])
      r
    end
  end

  def add(index_name, key, id) when is_binary(key) do
    index = get_or_create_index(index_name)
    db_id = :erlang.term_to_binary(id) #Reactive.CassandraEntityId.entity_db_id(id)
    {:ok,r} = :erlcass.execute(index.add, [key,db_id])
    r
  end

  def remove(index_name, keys, id) when is_list(keys) do
    index = get_or_create_index(index_name)
    db_id = :erlang.term_to_binary(id) #Reactive.CassandraEntityId.entity_db_id(id)
    for(key <- keys) do
      {:ok,r} = :erlcass.execute(index.remove, [key,db_id])
      r
    end
  end

  def remove(index_name, key, id) when is_binary(key) do
    index = get_or_create_index(index_name)
    db_id = :erlang.term_to_binary(id) #Reactive.CassandraEntityId.entity_db_id(id)
    {:ok,r} = :erlcass.execute(index.remove, [key,db_id])
    r
  end

  def update(index_name, old_keys, new_keys, id) when is_list(old_keys) and is_list(new_keys) do
    o = MapSet.new(old_keys)
    n = MapSet.new(new_keys)
    r = MapSet.difference(o,n)
    a = MapSet.difference(n,o)
    remove(index_name, MapSet.to_list(r), id)
    add(index_name, MapSet.to_list(a), id)
  end

  def update(index_name, old_key, new_key, id) when is_binary(old_key) and is_binary(new_key) do
    remove(index_name, old_key, id)
    add(index_name, new_key, id)
  end

  def find(index_name, key) do
    index = get_or_create_index(index_name)
    {:ok,r} = :erlcass.execute(index.find , [key])
    Enum.map(r,fn({x}) -> :erlang.binary_to_term(x) end)
  end
end