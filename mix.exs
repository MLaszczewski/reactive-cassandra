defmodule RactiveCassandra.Mixfile do
  use Mix.Project

  def project do
    [app: :reactive_cassandra,
     version: "0.1.0",
     elixir: ">= 1.2.3",
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger, :erlcass],
     mod: {RactiveCassandra, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [
      { :erlcass, github: "silviucpp/erlcass", ref: "ca4271f060e76ea61d5a9ad728a63e3f88614380" },
      { :confex, "~> 1.4.1" }
    ]
  end
end
